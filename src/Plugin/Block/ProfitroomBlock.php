<?php

namespace Drupal\profitroom\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Profitroom' Block.
 *
 * @Block(
 *   id = "profitroom_block",
 *   admin_label = @Translation("Profitroom block"),
 *   category = @Translation("Profitroom"),
 * )
 */
class ProfitroomBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */

  private $config;

  /**
   * Constructs a new ProfitroomBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfig($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->config('profitroom.settings');
    $site_name = $config->get('site_name');

    return [
      '#theme' => 'profit_room_block',
      '#site_name' => $site_name,
      '#attached' => [
        'library' => [
          'profitroom/bookingengine',
        ],
      ],
    ];
  }

  /**
   * Set Config Factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  protected function setConfig(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory;
  }

  /**
   * Retrieves a configuration object.
   *
   * @param string $conf
   *   The name of the configuration object to retrieve.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   An immutable configuration object.
   */
  protected function config($conf) {
    return $this->config->get($conf);
  }

}
