<?php

namespace Drupal\profitroom\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'ProfitroomField' formatter.
 *
 * @FieldFormatter(
 *   id = "profitroom",
 *   label = @Translation("Profitroom room reservation formatter"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class ProfitroomFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * Constructs a new ProfitroomFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->setConfig($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays Profitroom Identifier as reservation button');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $config = $this->config('profitroom.settings');
    $site_name = $config->get('site_name');

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'profit_room_field_formatter',
        '#value' => $item->value,
        '#site_name' => $site_name,
        '#attached' => [
          'library' => [
            'profitroom/bookingengine',
          ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * Set Config Factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  protected function setConfig(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory;
  }

  /**
   * Retrieves a configuration object.
   *
   * @param string $conf
   *   The name of the configuration object to retrieve.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   An immutable configuration object.
   */
  protected function config($conf) {
    return $this->config->get($conf);
  }

}
